<?php


final class Group extends ArrayIterator
{

    private static          $groupInstance = null;
    private         string  $filepath;

    // For a singleton class we need to set private/protected constructor
    protected function __construct( string $filepath )
    {
        try{
            $newData = array();
            
            // We will check if the file exists
            if( !is_file( $filepath ) ) 
                throw new InvalidArgumentException("Soubor s daty neexistuje.");
            
            // Static method of TextClass will return the field based on our
            // database file.
            $data = TextClass::getDataFromFile( $filepath );
            foreach ( $data as $key => $person )
            {
                if( $key == 0 ) continue;
                $newData[$key] = new User(
                    $key,
                    $person["firstname"],
                    $person["surname"],
                    $person["sex"],
                    $person["birthdate"]
                );
            }



            // Parent constructor solves the throwing Exceptions for us.
            // - InvalidArgumentException
            parent::__construct( $newData );
            $this->filepath = $filepath;
        } catch ( InvalidArgumentException $e ) {
            print_r($e);
        }

    }

    // If we do not want another instance, we also have forbid
    // to clone it.
    private function __clone() {}


    public static function getGroup( string $filepath ) : self
    {
        if( is_null(self::$groupInstance) )
            self::$groupInstance = new Group( $filepath );

        return self::$groupInstance;
    }


    // Get Person by his/her ID. In this moment, when the instance is prepared,
    // we only need to check it and return the value.
    public function getPersonById( int $id )
    {
        try{
            // We will check if the instance is not null
            if( is_null( self::$groupInstance ) )
                throw new Exception("Neexistuje instance třídy");

            // Does the person with this Id exists?
            if( $id >= count($this) )
                throw new Exception("Člověk s ID $id neexistuje.");

            // If the try block goes well, we return the existing user.
            return $this[$id];
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        return false;

    }


    // Finally we want to know how many men and women there are and get some
    // magical statistics (that hopefully will not be abused).
    public function getAllNumber() : int
    {
        return count($this);
    }

    public function getManNumber() : int
    {
        $manNumber = 0;
        foreach( $this as $person ){
            if( $person->getSex() == "M" ) $manNumber++;
        }
        return $manNumber;
    }

    public function getWomanNumber() : int
    {
        // Probably better would be take count( $this ) and
        // take $this->getManNumber() from it, but since there
        // are many genders today, this is more safe.
        $womanNumber = 0;
        foreach( $this as $person ){
            if( $person->getSex() == "F" ) $womanNumber++;
        }
        return $womanNumber;
    }


    // This function is computing ratio, dependently on given sex.
    public function getRatio( $sex ): float
    {
        $all = $this->getAllNumber();
        switch ( $sex ){
            case "M":
                $num = $this->getManNumber();
                break;
            case "F":
                $num = $this->getWomanNumber();
                break;
            default:
                $num = 0;
                break;
        }
        return round( $num / $all, 3);
    }

    // Percentage is 100 * ratio since the symbol % represents 10E-2.
    public function getPercentage( $sex ) : float
    {
        return 100 * $this->getRatio( $sex );
    }
}