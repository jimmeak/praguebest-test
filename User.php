<?php


class User
{

    private int     $id;
    private string  $firstname;
    private string  $surname;
    private string  $sex;
    private string  $birthdate;
    private int     $days;


    public function __construct
    (
        $id,
        $firstname,
        $surname,
        $sex,
        $birthdate
    )
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->surname = $surname;
        $this->setSex( $sex );
        $this->setBirthday( $birthdate );
        $this->setDaysOfLife();
    }


    // Getters for the User properties
    public function getId           () : int      {   return $this->id;          }
    public function getFirstName    () : string   {   return $this->firstname;   }
    public function getSurname      () : string   {   return $this->surname;     }
    public function getSex          () : string   {   return $this->sex;         }
    public function getBirthdate    () : string   {   return $this->birthdate;   }
    public function getDays         () : int      {   return $this->days;        }



    // Get the number of days in life
    private function setDaysOfLife() : void
    {
        // easy math for seconds -> minutes -> hours -> days
        // time() will return seconds
        $this->days = floor(( time() - strtotime( $this->getBirthdate() ) )/60/60/24);

    }


    // Set the sex
    private function setSex( string $sex ) : void
    {
        try{

            if( !CheckClass::checkSexFormat( $sex ) )
                throw new Exception("Pohlaví $sex není ve správném formátu: M nebo F.");
            $this->sex = $sex;

        } catch ( Exception $e ){
            echo "<pre>";
            echo $e->getMessage();
            echo "</pre>";
            exit();
        }
    }

    // Set the birthday
    private function setBirthday( string $birthdate ) : void
    {
        try {

            if( !CheckClass::checkBirthdateFormat( $birthdate ) )
                throw new Exception("Datum narození $birthdate není ve správném formátu");

            if( !CheckClass::checkBirthdate( $birthdate) )
                throw new Exception("Datum $birthdate v našem světě neexistuje.");

            $this->birthdate = $birthdate;

        } catch (Exception $e) {
            echo "<pre>";
            echo $e->getMessage();
            echo "</pre>";
            exit();
        }
    }





}