<?php


class TextClass
{

    public static function getDataFromFile( string $string ) : array
    {
        $time_start = microtime(true);

        $data = new SplFileObject($string);
        try {
            // Catch will stop the program if the file is not readable
            if (!$data->isReadable())
                throw new Exception("Soubor není čitelný.");

            $persons = array();

            foreach ($data as $line => $line_data) {
                // We skip the empty lines
                if( $line % 3 == 2) continue;

                // The user counting starts at 1
                $user_id = floor( $line/3 ) + 1;

               // Each thirds line 0, 3, 6, 9, ... contains name and surname
               if( $line % 3 == 0 ) {
                   $persons[$user_id]["firstname"]  =   self::widerExplode($line_data)[0];
                   $persons[$user_id]["surname"]    =   self::widerExplode($line_data)[1];
               }

               // Each thirds line 1, 4, 7, 10, ... contains sex and datebirth
               if( $line % 3 == 1 ) {
                   $persons[$user_id]["sex"]        =   self::changeSexFormat(self::widerExplode($line_data)[0]);
                   $persons[$user_id]["birthdate"]  =   self::widerExplode($line_data)[1];
               }

            }

            // in 0th position we can return some other data we might like
            $persons[0]["time_elapsed"] = round( microtime(true) - $time_start, 3);
            $persons[0]["all_users"]    = $user_id;

            $data = null;

        } catch (Exception $e) {
            echo "<pre>";
            print_r( $e->getMessage() );
            echo "</pre>";
            exit();
        }
        
        return $persons;
    }


    // We eplode the string with delimiter \s+
    public static function widerExplode( string $name ) : array
    {
        $name = str_replace("  ", " ", $name);
        return array_map( 'trim',explode(" ", $name) );
    }

    // Will return the same string without whitespaces \s.
    public static function killWhitespace( string $text ) : string
    {
        return preg_replace("/\s/m",'', $text);
    }

    // Will change the sex-format Muž -> M, Žena -> F,
    // anything else to one hell of a cool unicorn.
    public static function changeSexFormat( string $string ): string
    {
        $string = self::killWhitespace( $string );
        if( $string === "muž"  )    return "M";
        if( $string === "žena" )    return "F";
        return "A unicorn!";
    }


}