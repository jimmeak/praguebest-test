<?php


class TextClass
{

    public static function getDataFromFile( string $file_content ) : array
    {        
        
        $pattern = "/^(?<firstname>\p{L}.+)\s(?<surname>\p{L}.+)\n"
                    . "(?<sex>\p{L}.+)\s+(?<birthdate>\p{N}.+)$/m";

        preg_match_all($pattern, $file_content, $persons, PREG_SET_ORDER);
        
        // We want to change given data. We will also kill all the whitespace symbols
        // that we do not want in the output. So we would not bother the program with
        // more RAM memory, we will use pointer in foreach loop.
        foreach ($persons as &$person) {
            $person["firstname"]    = self::killWhitespace  ( $person["firstname"]   );
            $person["surname"]      = self::killWhitespace  ( $person["surname"]     );
            $person["sex"]          = self::changeSexFormat ( $person["sex"]         );
            $person["birthdate"]    = self::killWhitespace  ( $person["birthdate"]   );
        }
        return $persons;
    }

    // Will return the same string without whitespaces \s.
    public static function killWhitespace( string $string ) : string
    {
        return preg_replace("/\s/m",'', $string);
    }

    // Will change the sex-format Muž -> M, Žena -> F,
    // anything else to one hell of a cool unicorn.
    public static function changeSexFormat( string $string ): string
    {
        $string = self::killWhitespace( $string );
        if( $string === "muž"  )    return "M";
        if( $string === "žena" )    return "F";
        return "A unicorn!";
    }


}