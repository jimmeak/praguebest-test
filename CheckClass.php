<?php


class CheckClass
{

    public static function checkBirthdate( string $input ) : bool
    {
        // If the format is okay, then check the existence of the date
        if( self::checkBirthdateFormat($input) )
        {
            $field = explode(".", $input);

            // checkdate will return true if the date is a real time date,
            // if it is not, then it returns false.
            return checkdate($field[1], $field[0],$field[2]);
        }
        return false;
    }

    public static function checkBirthdateFormat( string $input ) : bool
    {
        // xx.xx.xxxx - does not check if the date exists, only format
        // We will also prohibit people born in 18th century or before.
        $pattern = "/^[0-3][0-9]\.[0-1][0-9]\.(18|19|20)[0-9]{2}$/";
        if( preg_match($pattern,$input) === 1 ) return true;
        return false;
    }

    public static function checkSexFormat( string $input ) : bool
    {
        // M or F
        $pattern = "/^[M|F]$/";
        if( preg_match($pattern,$input) === 1 ) return true;
        return false;
    }

}