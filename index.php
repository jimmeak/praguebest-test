<?php

// Just a file to see some results.

spl_autoload_register(
    function ($class){
        include_once($class . '.php');
    });

$group = Group::getGroup( "data.txt" );


echo '<h2> Group skrze ukazatel pole </h2>';
    echo '<pre>';
        echo "Vypisujeme print_r(\$group[1]): <br><br>";
        print_r($group[1]);
    echo '</pre>';

echo '<h2> Zastoupení mužů a žen </h2>';
    echo '<pre>';
        echo 'Celkem uživatelů ' . $group->getAllNumber() . '<br>';
        echo 'Počet mužů: ' . $group->getManNumber() . '.<br>';
        echo 'Počet žen: ' . $group->getWomanNumber() . '.<br>';
        echo 'Zastoupení mužů je ' . $group->getPercentage('M') . '&#37;.<br>';
        echo 'Zastoupení žen je ' . $group->getPercentage('F') . '&#37;.<br>';
    echo '</pre>';

echo '<h2> Group skrze foreach </h2>';

    echo '<pre>';
        foreach ($group as $person) {

            // For larger data we do not want to see them all on the display
            // it takes a lot of time.
            if( $person->getId() > 12 ) break;

            echo $person->getId() . "\t  (" . $person->getSex() . ") \t\t";
            echo $person->getBirthdate() . " (" . $person->getDays() ." days)\t\t";
            echo $person->getFirstname() . " " . $person->getSurname() . "\t";
            echo "<br>";
        }
    echo '</pre>';


